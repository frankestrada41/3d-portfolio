import React, { lazy, useEffect } from 'react'
import { Suspense } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import './App.scss';

const HomeApp = lazy(() => import('./pages/HomeApp/HomeApp'))
const About = lazy(() => import('./pages/About/About'))
const Portfolio = lazy(() => import('./pages/Portfolio/Portfolio'))
const Contact = lazy(() => import('./pages/Contact/Contact'))
const Photos = lazy(() => import('./pages/Photos/Photos'))
const Colors = lazy(() => import('./pages/Colors/Colors'))
const Apicall = lazy(() => import('./pages/Apicall/Apicall'))


function App() {
  const style = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
  };
  useEffect(() => {
    const getWeather = async () => {
      let res = await fetch(
        "https://rickandmortyapi.com/api/character"
      );
      let apiWeather = await res.json();
      console.log(apiWeather)
    };

    getWeather();
  }, []);


  return (
    < div className="App">

      <Router>
        <Suspense fallback={<div style={style}><img src="https://media.giphy.com/media/N256GFy1u6M6Y/giphy.gif" alt="" /></div>}>
          <Switch>
            <Route
              exact={true}
              path='/'
              component={props => <HomeApp {...props} />}
            />
            <Route
              exact={true}
              path={'/About'}
              component={props => <About {...props} />}
            />
            <Route
              exact={true}
              path={'/Portfolio'}
              component={props => <Portfolio {...props} />}
            />
            <Route
              exact={true}
              path={'/Contact'}
              component={props => <Contact {...props} />}
            />
            <Route
              exact={true}
              path={'/Photos'}
              component={props => <Photos {...props} />}
            />
            <Route
              exact={true}
              path={'/Colors'}
              component={props => <Colors {...props} />}
            />
            <Route
              exact={true}
              path={'/Apicall'}
              component={props => <Apicall {...props} />}
            />
          </Switch>
        </Suspense>

      </Router>

    </div>
  );
}

export default App;
