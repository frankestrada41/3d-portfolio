import React from 'react';
import { NavLink } from 'react-router-dom';
import './Navbar.scss'

const Navbar = () => {
  return (
    <div className="navbar-container">
      <div>
        <NavLink to="/About" activeClassName='active-item'>
          About me
        </NavLink>
      </div>
      <NavLink to="/Contact">
        Contact
      </NavLink>
      <NavLink to="/Photos">
        Portfolio
      </NavLink>
      <NavLink to="/Colors">
        Colors
      </NavLink>
      <div>
        <NavLink to="/Apicall" activeClassName='active-item'>
          Api call
        </NavLink>
      </div>
    </div>
  )
}

export default Navbar