
import React, { useRef } from 'react'
import { useGLTF } from '@react-three/drei'
import { OrbitControls } from "@react-three/drei"

export default function Girl(props) {
  const group = useRef()
  const { nodes, materials } = useGLTF('/girl.gltf')

  return (
    <group ref={group} {...props} dispose={null}>
      <group rotation={[-Math.PI / 2, 0, 0]} scale={0.02}>
        <group rotation={[Math.PI / 2, 0, 0]}>
          <primitive object={nodes._rootJoint} />
          <skinnedMesh
            geometry={nodes.Object_7.geometry}
            material={materials.CARA_1}
            skeleton={nodes.Object_7.skeleton}
          />
          <skinnedMesh
            geometry={nodes.Object_8.geometry}
            material={materials.TORSO}
            skeleton={nodes.Object_8.skeleton}
          />
          <skinnedMesh
            geometry={nodes.Object_9.geometry}
            material={materials.PIERNSD}
            skeleton={nodes.Object_9.skeleton}
          />
          <skinnedMesh
            geometry={nodes.Object_10.geometry}
            material={materials.BRAZOS}
            skeleton={nodes.Object_10.skeleton}
          />
          <skinnedMesh
            geometry={nodes.Object_11.geometry}
            material={materials.OJOS1}
            skeleton={nodes.Object_11.skeleton}
          />
          <skinnedMesh
            geometry={nodes.Object_12.geometry}
            material={materials.OJOS2}
            skeleton={nodes.Object_12.skeleton}
          />
          <skinnedMesh
            geometry={nodes.Object_13.geometry}
            material={materials.PESTAAS}
            skeleton={nodes.Object_13.skeleton}
          />
          <skinnedMesh
            geometry={nodes.Object_15.geometry}
            material={materials['ARM_1.1']}
            skeleton={nodes.Object_15.skeleton}
          />
          <skinnedMesh
            geometry={nodes.Object_16.geometry}
            material={materials['ARM_1.2']}
            skeleton={nodes.Object_16.skeleton}
          />
          <skinnedMesh
            geometry={nodes.Object_17.geometry}
            material={materials.Default}
            skeleton={nodes.Object_17.skeleton}
          />
          <skinnedMesh
            geometry={nodes.Object_18.geometry}
            material={materials['ARM_2.1']}
            skeleton={nodes.Object_18.skeleton}
          />
          <skinnedMesh
            geometry={nodes.Object_19.geometry}
            material={materials['ARM_2.2']}
            skeleton={nodes.Object_19.skeleton}
          />
          <skinnedMesh
            geometry={nodes.Object_20.geometry}
            material={materials['CO-CA_1.1']}
            skeleton={nodes.Object_20.skeleton}
          />
          <skinnedMesh
            geometry={nodes.Object_21.geometry}
            material={materials['CO-CA_1.2']}
            skeleton={nodes.Object_21.skeleton}
          />
          <OrbitControls />
        </group>
      </group>
    </group>
  )
}

useGLTF.preload('/girl.gltf')
