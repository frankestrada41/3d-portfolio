import React from 'react';
import { NavLink } from 'react-router-dom';

import Arrow from '../../assets/proyect-fotos/backarrow.svg'

import './HomeButton.scss'

const HomeButton = () => {
  return (
    <div className="homebutton-container">
      <NavLink to="/" activeClassName='active-item'>
        <img src={Arrow} alt="Home" className="arrow" />
      </NavLink>
    </div>
  )
}

export default HomeButton