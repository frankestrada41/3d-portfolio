import React, { useRef } from "react";
import * as THREE from 'three'
import { useLoader, useFrame } from '@react-three/fiber'
import { TextureLoader } from 'three'
import { OrbitControls, Stars } from "@react-three/drei";

import EarthDayMap from '../../assets/textures/8k_earth_daymap.jpeg'
import EarthNormalMap from '../../assets/textures/8k_earth_normal_map.jpeg'
import EarthCloudsMap from '../../assets/textures/8k_earth_clouds.jpg'
import EarthSpecularMap from '../../assets/textures/8k_earth_specular_map.jpeg'

export function Earth(props) {

  const [colorMap, normalMap, specularMap, cloudsMap] = useLoader(TextureLoader, [EarthDayMap, EarthNormalMap, EarthSpecularMap, EarthCloudsMap]);

  const earthRef = useRef();
  const cloudRef = useRef();

  useFrame(({ clock }) => {
    const elapsedTime = clock.getElapsedTime();

    earthRef.current.rotation.y = elapsedTime / 8;
    cloudRef.current.rotation.y = elapsedTime / 8;
  });


  return <>
    {/* <ambientLight intensity={1} /> */}
    <pointLight color="#f6f3ea" position={[2, 1, 3]} intensity={2.5} />
    <pointLight color="#f6f3ea" position={[-2, -1, 0.5]} intensity={0.5} />
    <Stars
      radius={300}
      depth={6}
      count={10000}
      factor={7}
      saturation={5}
      fade={true}
    />
    <mesh ref={cloudRef}>
      <sphereGeometry args={[2.005, 32, 32]} />
      <meshPhongMaterial
        map={cloudsMap}
        opacity={0.4}
        depthWrite={true}
        transparent={true}
        side={THREE.DoubleSide} />
    </mesh>
    <mesh ref={earthRef}>
      <sphereGeometry args={[2, 32, 32]} />
      <meshPhongMaterial specularMap={specularMap} />
      <meshStandardMaterial
        map={colorMap}
        normalMap={normalMap}
        metalness={0.3}
        roughness={0.7}
      />
      <OrbitControls />
    </mesh>

  </>

}