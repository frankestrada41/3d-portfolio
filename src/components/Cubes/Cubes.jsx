import React, { useState } from 'react';
import { Canvas, useLoader } from '@react-three/fiber'
import { Stars, OrbitControls, Reflector, Text } from '@react-three/drei'
// import { useSpring, a } from 'react-spring'
import { TextureLoader } from "three";
import { Box } from '../../components'
import Linkedin from '../../assets/textures/linkedin.jpeg'
import Gitlab from '../../assets/textures/gitlab.png'
import './Cubes.scss';


// softShadow para que sean mas realistas.
//  Ahora desactivado porque consume mucho. Hay que inportarlo de drei
// softShadows();

const Plane = () => {
  return (
    <>
      {/* <mesh receiveShadow castShadow rotation={[-Math.PI / 2, 0, 0]} position={[0, -2.009, 0]} >
        <planeBufferGeometry attach="geometry" args={[21, 21]} />
        <meshStandardMaterial attach="material" opacity={0.5} color="#800080" />
      </mesh> */}
      <Reflector resolution={1080} args={[100, 1000]} mirror={0.7} mixBlur={0} mixStrength={2} rotation={[-Math.PI / 2, 0, Math.PI / 2]} blur={[100, 100]} position={[0, -2, 0]}>
        {(Material, props) => <Material color="#a0a0a0" metalness={0.5} normalScale={[1, 1]} {...props} />}
      </Reflector>
    </>
  )
}

const Cubes = () => {
  const [inTexture, gitTexture] = useLoader(TextureLoader, [Linkedin, Gitlab])
  const [textClicked, setTextClicked] = useState(false);
  const [textClicked2, setTextClicked2] = useState(false);

  const lnkUrl = 'https://www.linkedin.com/in/frankestradagonzalez/'
  const gitUrl = 'https://gitlab.com/frankestrada41'

  const textClick = () => {
    setTextClicked(!textClicked)
    window.location.href = gitUrl
  }

  const textClick2 = () => {
    setTextClicked2(!textClicked2)
    window.location.href = lnkUrl
  }

  return (
    <>
      <div className="canvas-container">
        <Canvas shadows camera={{ position: [0, 2, 10], fov: 100 }}>
          <Stars
            radius={300}
            depth={6}
            count={10000}
            factor={7}
            saturation={5}
            fade={true}
          />
          <pointLight intensity={0.5} position={[2, -5, 10]} />
          <directionalLight
            castShadow
            intensity={1.2}
            position={[0, 10, 0]}
            shadow-mapSize-width={1024}
            shadow-mapSize-height={1024}
            shadow-camera-far={50}
            shadow-camera-top={10}
            shadow-camera-bottom={-10}
            shadow-camera-left={-10}
            shadow-camera-right={10}
          />

          <group>
            <Plane />
            <Box color={"lightblue"} position={[-3, 0, 0]} speed={3} map={inTexture} text={textClicked2} id={'link'} />
            <Box color={"lightblue"} position={[3, 0, 0]} speed={3} map={gitTexture} text2={textClicked} id={'gitlab'} />
          </group>

          <Text onClick={textClick}
            anchorX={-1.5} // default
            anchorY={-2.5} // default
            fontSize={1}
            color={textClicked ? "green" : "white"}
            className="text"
          >
            Gitlab
          </Text>
          <Text onClick={textClick2}
            color={textClicked2 ? "green" : "white"}
            anchorX={4.7} // default
            anchorY={-2.5} // default
            fontSize={1}
            className="text"
          >
            Linkedin
          </Text>
          <OrbitControls />
        </Canvas>
      </div>
    </>
  )
}

export default Cubes