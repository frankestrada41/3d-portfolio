import React from 'react';
import { Reflector } from '@react-three/drei'
const Mirror = ({ position, rotation, args }) => {
  return (
    <>
      <Reflector rotation={rotation} resolution={1080} args={args} mirror={1} mixBlur={0} mixStrength={5} blur={[100, 100]} position={position}>
        {(Material, props) => <Material color="#a0a0a0" metalness={0.5} normalScale={[1, 1]} {...props} />}
      </Reflector>
    </>
  )
}

export default Mirror