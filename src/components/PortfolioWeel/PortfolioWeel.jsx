import React, { useState, Suspense, useRef, useEffect } from 'react';
import { useLoader, useFrame } from '@react-three/fiber'
import { TextureLoader } from 'three'
// import { RoundedBox } from '@react-three/drei'
import Got from '../../assets/proyect-fotos/got.png'
import Piano from '../../assets/proyect-fotos/piano.png'
import Deepdark from '../../assets/proyect-fotos/deepdark.png'
import RM from '../../assets/proyect-fotos/r&m.png'

const PortfolioWeel = () => {
  const [counter, setCounter] = useState(null)

  useEffect(() => {

    document.addEventListener('wheel', remover, true)
    document.addEventListener('touchmove', remover, true)

    function remover(e) {
      document.removeEventListener('weel', remover, true)
      document.removeEventListener('touchmove', remover, true)
      if (e.deltaY <= 2000) {
        setCounter(e.deltaY % 0.02)
      }

    }

  }, [])



  const scrolMoveBox = useRef();
  console.log(scrolMoveBox.current)
  useFrame(() => {
    scrolMoveBox.current.rotation.y = scrolMoveBox.current.rotation.y + counter

  })
  const proyect1 = useLoader(TextureLoader, Got);
  const proyect2 = useLoader(TextureLoader, Piano);
  const proyect3 = useLoader(TextureLoader, Deepdark);
  const proyect4 = useLoader(TextureLoader, RM);


  const stopCube = () => {
    setCounter(0)
  }
  return (
    <>
      <Suspense fallback={null} >
        <mesh ref={scrolMoveBox} castShadow onClick={stopCube}>
          <boxBufferGeometry attach="geometry" args={[3, 3, 3]} />

          <meshStandardMaterial attachArray="material" map={proyect1} />
          <meshStandardMaterial attachArray="material" map={proyect2} />
          <meshStandardMaterial attachArray="material" color="black" />
          <meshStandardMaterial attachArray="material" color="black" />
          <meshStandardMaterial attachArray="material" map={proyect3} />
          <meshStandardMaterial attachArray="material" map={proyect4} />

        </mesh>
      </Suspense>
    </>
  )
}

export default PortfolioWeel