import React, { useState, useEffect, useRef } from 'react';
import { TextureLoader } from "three";
import { useLoader, useFrame } from '@react-three/fiber'

import { MeshWobbleMaterial } from '@react-three/drei';
import AnimationsProyect from '../../assets/proyect-fotos/animationsPlane.png'
import Got from '../../assets/proyect-fotos/got.png'
import Piano from '../../assets/proyect-fotos/piano.png'
import Deepdark from '../../assets/proyect-fotos/deepdark.png'
import RM from '../../assets/proyect-fotos/r&m.png'

import waternormal from '../../assets/textures/water.png'
import waterspecular from '../../assets/textures/water-specular.png'

import './Gallery.scss'
const Gallery = ({
  setImgGeterHover,
  setImgGeterHover2,
  setImgGeterHover3,
  setImgGeterHover4,
  setImgGeterHover5

}) => {

  const [camY, setCamY] = useState(0);

  const [rotater, setRotater] = useState(-0.2);
  const [centerImg, setCenterImg] = useState(0.7);

  const [args, setArgs] = useState([1, 1, 1]);
  const [args1, setArgs1] = useState([1, 1, 1]);
  const [args2, setArgs2] = useState([1, 1, 1]);
  const [args3, setArgs3] = useState([1, 1, 1]);
  const [args4, setArgs4] = useState([1, 1, 1]);




  const [mouseFollower, setMouseFollower] = useState(null);
  const [mouseFollowerY, setMouseFollowerY] = useState(null);

  const proyect0 = useLoader(TextureLoader, AnimationsProyect)
  const proyect1 = useLoader(TextureLoader, Got);
  const proyect2 = useLoader(TextureLoader, Piano);
  const proyect3 = useLoader(TextureLoader, Deepdark);
  const proyect4 = useLoader(TextureLoader, RM);

  const waterNormalMap = useLoader(TextureLoader, waternormal);
  const waterSpecularMap = useLoader(TextureLoader, waterspecular);

  useEffect(() => {
    // console.log(window.innerWidth)
    if (window.innerWidth < 900) {
      setRotater(0)
      setCenterImg(0)
    } else {
      setRotater(-0.2)
      setCenterImg(0.7)
    }
  }, [])

  window.addEventListener('wheel', remover);
  function remover(e) {
    window.removeEventListener('wheel', remover);
    if (e.deltaY >= 0) {
      setCamY(camY + 0.07)
    }
    if (e.deltaY < 0) {
      setCamY(camY - 0.07)
    }

  }

  useEffect(() => {

    document.addEventListener('mousemove', followMouse);
    function followMouse(e) {
      document.removeEventListener('mousemove', followMouse);
      if (e.pageX && e.pageX > 600) {
        setMouseFollower(e.pageX - 600)
      }
      if (e.pageX && e.pageX < 600) {
        setMouseFollower(e.pageX - (601))
      }
      if (e.pageX && e.pageY > 600) {
        setMouseFollowerY(e.pageY - 600)
      }
      if (e.pageX && e.pageY < 600) {
        setMouseFollowerY(e.pageY - (601))
      }
      else {
        setCamY(e.clientY % 0.000001)
        // console.log(camY)
      }
    }
  }, [])

  const getMousePosition = () => {
    setArgs([1.3, 1.3, 1.3])
    setImgGeterHover(true)

  }

  const deleteMouse = () => {
    setArgs([1, 1, 1])
    setImgGeterHover(false)

  }

  const getMousePosition1 = () => {
    setArgs1([1.3, 1.3, 1.3])
    setImgGeterHover2(true)

  }

  const deleteMouse1 = () => {
    setArgs1([1, 1, 1])
    setImgGeterHover2(false)

  }

  const getMousePosition2 = () => {
    setArgs2([1.3, 1.3, 1.3])
    setImgGeterHover3(true)
  }

  const deleteMouse2 = () => {
    setArgs2([1, 1, 1])
    setImgGeterHover3(false)
  }

  const getMousePosition3 = () => {
    setArgs3([1.3, 1.3, 1.3])
    setImgGeterHover4(true)
  }

  const deleteMouse3 = () => {
    setArgs3([1, 1, 1])
    setImgGeterHover4(false)
  }

  const getMousePosition4 = () => {
    setArgs4([1.3, 1.3, 1.3])
    setImgGeterHover5(true)
  }

  const deleteMouse4 = () => {
    setArgs4([1, 1, 1])
    setImgGeterHover5(false)
  }

  const plane1 = useRef();
  const plane2 = useRef();
  const plane3 = useRef();
  const plane4 = useRef();
  const plane5 = useRef();

  useFrame(() => {
    plane1.current.rotation.y = -0.3 + mouseFollower / 5000
    plane1.current.rotation.x = mouseFollowerY / 5000
    plane2.current.rotation.y = -0.3 + mouseFollower / 5000
    plane2.current.rotation.x = mouseFollowerY / 5000
    plane3.current.rotation.y = -0.3 + mouseFollower / 5000
    plane3.current.rotation.x = mouseFollowerY / 5000
    plane4.current.rotation.y = -0.3 + mouseFollower / 5000
    plane4.current.rotation.x = mouseFollowerY / 5000
    plane5.current.rotation.y = -0.3 + mouseFollower / 5000
    plane5.current.rotation.x = mouseFollowerY / 5000
  })



  // Gets video for adding as a texture
  // const [video] = useState(() => {
  //   const vid = document.createElement('video');
  //   vid.src = url;
  //   vid.crossOrigin = "Anonymus";
  //   vid.loop = true;
  //   vid.muted = true;
  //   vid.play();
  //   return vid
  // })

  return (
    <>
      <group rotation={[0, 0, rotater]} >

        <mesh ref={plane1} castShadow position={[centerImg, 1 + camY, 0]} rotation={[0, -0.3, 0]} onPointerOver={getMousePosition} onPointerLeave={deleteMouse} >
          <planeBufferGeometry attach="geometry" args={args} />
          <meshStandardMaterial attach="material" opacity={1} map={proyect0} />
        </mesh>

        <mesh ref={plane2} castShadow position={[centerImg, -0.2 + camY, 0]} rotation={[0, -0.3, 0]} onPointerOver={getMousePosition1} onPointerLeave={deleteMouse1}>
          <planeBufferGeometry attach="geometry" args={args1} />
          <meshStandardMaterial attach="material" opacity={1} map={proyect1} />
        </mesh>
        <mesh ref={plane3} castShadow position={[centerImg, -1.4 + camY, 0]} rotation={[0, -0.3, 0]} onPointerOver={getMousePosition2} onPointerLeave={deleteMouse2}>
          <planeBufferGeometry attach="geometry" args={args2} />
          <meshStandardMaterial attach="material" opacity={1} map={proyect2} />
        </mesh>
        <mesh ref={plane4} castShadow position={[centerImg, -2.6 + camY, 0]} rotation={[0, -0.3, 0]} onPointerOver={getMousePosition3} onPointerLeave={deleteMouse3}>
          <planeBufferGeometry attach="geometry" args={args3} />
          <meshStandardMaterial attach="material" opacity={1} map={proyect3} />
        </mesh>
        <mesh ref={plane5} castShadow position={[centerImg, -3.8 + camY, 0]} rotation={[0, -0.3, 0]} onPointerOver={getMousePosition4} onPointerLeave={deleteMouse4}>
          <planeBufferGeometry attach="geometry" args={args4} />
          <meshStandardMaterial attach="material" opacity={1} map={proyect4} />
        </mesh>

        {/* <mesh position={[0, 0, -10]} receiveShadow>
          <planeGeometry attach="geometry" args={[100, 100, 100]} />
          <meshStandardMaterial emissive="white" side={THREE.DoubleSide}>
            <videoTexture attach="map" args={[video]} />
            <videoTexture attach="emissiveMap" args={[video]} />
          </meshStandardMaterial>
        </mesh> */}

        <mesh position={[0, 0, -10]} receiveShadow>
          <planeGeometry attach="geometry" args={[100, 100, 10, 100]} />
          <meshPhongMaterial specularMap={waterSpecularMap} />
          <MeshWobbleMaterial
            attach="material"
            color="#221D24"
            factor={0.1} // Strength, 0 disables the effect (default=1)
            speed={1} // Speed (default=1)
            roughness={0}
            metalness={0.7}
            normalMap={waterNormalMap}
          />
        </mesh>
      </group>
    </>
  );
}

export default Gallery;