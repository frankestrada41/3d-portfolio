import React from 'react';
import { MeshWobbleMaterial } from '@react-three/drei'
import * as THREE from 'three'
import { useLoader } from '@react-three/fiber';

import wavesNormlMap from '../../assets/textures/water.png'


const WobblePlane = ({ position, args, color }) => {
  const waveNP = useLoader(THREE.TextureLoader, wavesNormlMap)
  return (
    <>
      <mesh position={position} receiveShadow>
        <planeGeometry attach="geometry" args={args} />
        <meshPhongMaterial specularMap={waveNP} />
        <MeshWobbleMaterial
          side={THREE.DoubleSide}
          attach="material"
          color={color}
          factor={0.3} // Strength, 0 disables the effect (default=1)
          speed={1} // Speed (default=1)
          roughness={0}
          metalness={0.7}
          normalMap={waveNP}
        />
      </mesh>
    </>
  )
}

export default WobblePlane