import Navbar from "./Navbar/Navbar";
import { Earth } from "./Earth/Earth";
import Planet1 from './Planet1/Planet1'
import HomeButton from './HomeButton/HomeButton'
import Proyect1 from './Proyect1/Proyect1'
import Proyect2 from './Proyect2/Proyect2'
import Cubes from './Cubes/Cubes'
import Box from './Box/Box'
import Mirror from './Mirror/Mirror'
import Plane from './Plane/Plane'
import PortfolioWeel from './PortfolioWeel/PortfolioWeel'
import Gallery from "./Gallery/Gallery";
import WobblePlane from './WobblePlane/WobblePlane'
import Sphere from './Sphere/Sphere'
import Apicanvas from './Apicanvas/Apicanvas'



export {
  Navbar,
  Earth,
  Planet1,
  HomeButton,
  Proyect1,
  Proyect2,
  Cubes,
  Box,
  Mirror,
  Plane,
  PortfolioWeel,
  Gallery,
  WobblePlane,
  Sphere,
  Apicanvas,
}