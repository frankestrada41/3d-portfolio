import React, { useRef, useState } from 'react'
import { useFrame } from '@react-three/fiber'
import { MeshWobbleMaterial } from '@react-three/drei'
const Box = ({ position, args, speed, map, text, text2, id }) => {
  const mesh = useRef(null);
  useFrame(() => { mesh.current.rotation.x = mesh.current.rotation.y += 0.01 })

  const [isLarge, setisLarge] = useState(false);

  // const expand = useSpring({
  //   scale: isLarge ? [1.4, 1.4, 1.4] : [1, 1, 1]
  // });
  const clickBox = () => {
    setisLarge(!isLarge)
    if (id === 'link') {
      window.location.href = 'https://www.linkedin.com/in/frankestradagonzalez/'
    }
    if (id === 'gitlab') {
      window.location.href = 'https://gitlab.com/frankestrada41'
    }
  }

  return (
    <mesh
      onClick={clickBox}
      scale={isLarge || text || text2 ? [2, 2, 2] : [1, 1, 1]}
      castShadow
      position={position}
      ref={mesh}

    >
      <boxBufferGeometry attachment="geometry" args={args} />
      <MeshWobbleMaterial attachment="material" map={map} speed={speed} factor={0.8} />

    </mesh>
  )
}

export default Box