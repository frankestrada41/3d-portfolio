import React, { useRef } from 'react';
import { useFrame } from '@react-three/fiber'

const Sphere = ({ args, position, map, normalMap }) => {
  const planet2 = useRef();
  let isBack = false;
  let isBackZ = false;

  useFrame(({ clock }) => {
    // const elapsedTime = clock.getElapsedTime();
    // console.log(planet2.current.position.z);

    if (
      planet2.current.position.x < 8 && !isBack) {
      planet2.current.position.x = planet2.current.position.x + 0.03125
    }
    if (planet2.current.position.x === 8) {
      isBack = true;
    }

    if (planet2.current.position.x > -8 && isBack === true) {
      planet2.current.position.x = planet2.current.position.x - 0.03125
    }

    if (planet2.current.position.x === -8) {
      isBack = false;
    }








    if (planet2.current.position.z < 8 && !isBackZ) {
      planet2.current.position.z = planet2.current.position.z + 0.03125
    }

    if (planet2.current.position.z === 8) {
      isBackZ = true;
    }

    if (planet2.current.position.z > -8 && isBackZ === true) {
      planet2.current.position.z = planet2.current.position.z - 0.03125
    }

    if (planet2.current.position.z === -8) {
      isBackZ = false;
    }


  })
  return (
    <mesh ref={planet2} position={position}>
      <sphereGeometry args={args} />
      <meshStandardMaterial
        map={map}
        normalMap={normalMap} />
    </mesh>
  );
}

export default Sphere