import React from 'react';

import { Canvas } from '@react-three/fiber';
import { OrbitControls } from '@react-three/drei'
import { WobblePlane } from '../../components'


import './Proyect2.scss'

const Proyect2 = () => {
  return (
    <div className="proyect2-container">

      <div className="proyect1-container__block1">

        <h2 className="proyect2-container__title2">Turquoise</h2>
        <p className="proyect2-container__description2">Turquoise (/ˈtɜːrkɔɪz, -kwɔɪz/) is a
          cyanish-green color, based on the gem of the same name. The word turquoise dates to
          the 17th century and is derived from the French turquois meaning "Turkish" because the
          mineral was first brought to Europe through Turkey from mines in the historical
          Khorasan province of Iran (Persia) and Afghanistan.The first recorded use of turquoise
          s a color name in English was in 1573.</p>
      </div>

      <div className="proyect1-container__block2">
        <Canvas camera={{ position: [3, -3, 2] }}>
          <pointLight position={[0, 0, 4]} color="#00FBFF" />
          <pointLight position={[0, 0, -4]} />
          <WobblePlane position={[0, 0, 0]} args={[3.5, 3.5, 3.5]} color={"#00FBFF"} />
          <WobblePlane position={[0, 4, 0]} args={[3.5, 3.5, 3.5]} color={"#00FBFF"} />
          <WobblePlane position={[0, 8, 0]} args={[3.5, 3.5, 3.5]} color={"#00FBFF"} />
          <OrbitControls enableZoom={false} />
        </Canvas>
      </div>

    </div>
  )
}

export default Proyect2;