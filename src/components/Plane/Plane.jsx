import React from 'react';
// import { Reflector } from '@react-three/drei'
const Plane = ({ position, rotation, args, color, map }) => {
  return (
    <>
      <mesh receiveShadow rotation={rotation} position={position} >
        <planeBufferGeometry attach="geometry" args={args} />
        <meshStandardMaterial attach="material" opacity={0.5} color={color} map={map} />
      </mesh>
    </>
  )
}

export default Plane