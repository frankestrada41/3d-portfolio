import React from 'react';
import { Canvas } from '@react-three/fiber'
import { Plane } from '../../components'
import { OrbitControls } from '@react-three/drei'

import './Apicanvas.scss'


const Apicanvas = () => {

  return (
    <div className="apicanvas-container">

      <Canvas>
        <ambientLight intensity={1} />
        <Plane position={[2, 0, 0]} args={[1, 1]} />
        <Plane position={[0, 0, 0]} args={[1, 1]} />
        <Plane position={[-2, 0, 0]} args={[1, 1]} />
        <OrbitControls />
      </Canvas>
    </div>
  );
};

export default Apicanvas;