
import React from "react";

import './Proyect1.scss';
import { Canvas } from '@react-three/fiber';
import { OrbitControls } from '@react-three/drei'
import { WobblePlane } from '../../components'




const Proyect1 = () => {




  return (
    <div className="proyect1-container">
      <div className="proyect1-container__block1">

        <h2 className="proyect1-container__title">Red</h2>
        <p className="proyect1-container__description">Red is the color at the long wavelength
          end of the visible spectrum of light, next to orange and opposite violet. It has a
          dominant wavelength of approximately 625–740 nanometres.[1] It is a primary color in
          the RGB color model and a secondary color (made from magenta and yellow) in the CMYK
          color model, and is the complementary color of cyan. Reds range from the brilliant
          yellow-tinged scarlet and vermillion to bluish-red crimson, and vary in shade from the
          pale red pink to the dark red burgundy.</p>

      </div>

      <div className="proyect1-container__block2">
        <Canvas camera={{ position: [3, -3, 2] }}>
          <pointLight position={[0, 0, 4]} color="red" />
          <pointLight position={[0, 0, -4]} color="red" />
          <WobblePlane position={[0, 0, 0]} args={[3.5, 3.5, 3.5]} />
          <WobblePlane position={[0, 4, 0]} args={[3.5, 3.5, 3.5]} />
          <WobblePlane position={[0, 8, 0]} args={[3.5, 3.5, 3.5]} />
          <OrbitControls enableZoom={false} />
        </Canvas>
      </div>

    </div >
  )
}

export default Proyect1;