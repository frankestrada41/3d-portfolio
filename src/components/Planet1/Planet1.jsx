import React, { useRef, useState, useEffect } from 'react';
import { OrbitControls, Stars } from '@react-three/drei'
import { useFrame, useLoader } from '@react-three/fiber'
import { TextureLoader } from 'three'

import Lion from '../../assets/textures/lion.jpg'
import PlanetNormal from '../../assets/textures/planet1_normalmap.jpg'
const Planet1 = () => {

  const [lionMap, normalMap] = useLoader(TextureLoader, [Lion, PlanetNormal])

  const [mousePosition, setMousePosition] = useState({
    x: null,
    y: null
  });


  useEffect(() => {
    document.addEventListener("mousemove", (e) => {
      setMousePosition({ x: e.pageX, y: e.pageY })
    })

  }, [])

  const planetRef = useRef();

  useFrame(({ clock }) => {
    planetRef.current.rotation.y = mousePosition.y / 1000
    planetRef.current.rotation.x = mousePosition.x / 1000

    const elapsedTime = clock.getElapsedTime();

    planetRef.current.rotation.y = elapsedTime / 8;
  })

  return (
    <>
      <Stars
        radius={100}
        depth={6}
        count={10000}
        factor={7}
        saturation={5}
        fade={true}
      />
      <mesh ref={planetRef}>
        <sphereGeometry args={[2, 32, 100]} />
        <meshStandardMaterial
          map={lionMap}
          normalMap={normalMap}
          metalness={0.3}
          roughness={0.7}
        />
        <OrbitControls />
      </mesh>
    </>
  )
}

export default Planet1