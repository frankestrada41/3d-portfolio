import React, { useState } from 'react';
import { Canvas } from '@react-three/fiber'
// import { OrbitControls } from '@react-three/drei'
import { Gallery, Navbar, HomeButton } from '../../components'
import './Photos.scss'



const Photos = () => {
  const [imgGeterHover, setImgGeterHover] = useState(false)
  const [imgGeterHover2, setImgGeterHover2] = useState(false)
  const [imgGeterHover3, setImgGeterHover3] = useState(false)
  const [imgGeterHover4, setImgGeterHover4] = useState(false)
  const [imgGeterHover5, setImgGeterHover5] = useState(false)



  return (
    <>
      <HomeButton />
      <Navbar />
      <div className="title-container">
        <div className="main-tittle">Welcome to my portfolio:</div>
        {imgGeterHover === true && < div className="main-tittle__1">Rave Animations</div>}
        {imgGeterHover2 === true && < div className="main-tittle__1">Game of Thrones</div>}
        {imgGeterHover3 === true && < div className="main-tittle__1">PianoSynth</div>}
        {imgGeterHover4 === true && < div className="main-tittle__1">Deep Dark E-Commerce</div>}
        {imgGeterHover5 === true && < div className="main-tittle__1">Rick & Morty </div>}
      </div>


      <div className="photos-container">
        <Canvas shadows camera={{ position: [0, 0, 1], fov: 120 }} >
          {/* <ambientLight intensity={4} /> */}
          <pointLight position={[-3, 0, 10]} intensity={0.5} />
          <pointLight position={[3, 4, 10]} intensity={0.5} />
          <pointLight position={[7, -12, 1]} intensity={0.5} />
          <directionalLight position={[0, 0, 55]} intensity={0.5} />
          <Gallery
            setImgGeterHover={setImgGeterHover}
            setImgGeterHover2={setImgGeterHover2}
            setImgGeterHover3={setImgGeterHover3}
            setImgGeterHover4={setImgGeterHover4}
            setImgGeterHover5={setImgGeterHover5}
          />
          {/* <OrbitControls /> */}
        </Canvas>
      </div >
    </>
  );
}

export default Photos