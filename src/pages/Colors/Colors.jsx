import React from 'react';
import { Proyect1, Proyect2, HomeButton, Navbar } from '../../components'
const Colors = () => {
  return (
    <>
      <HomeButton />
      <Navbar />
      <Proyect1 />
      <Proyect2 />
    </>
  );
}

export default Colors;