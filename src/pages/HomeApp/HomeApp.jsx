import React from 'react';
import { Canvas } from '@react-three/fiber'
import { Suspense } from 'react';
import { Earth, Navbar } from '../../components'

import './HomeApp.scss'


const HomeApp = () => {
  return (
    <>

      <div className="canvas-container">

        <div className="home-container">
          <Navbar />
          <h1 className="home-container__title">Welcome!, <br /> I'm Frank, <br /> Full Stack Developer!</h1>
        </div>
        <Canvas>
          <Suspense fallback={null}>
            <Earth />
          </Suspense>
        </Canvas>

      </div>

    </>
  )
}

export default HomeApp