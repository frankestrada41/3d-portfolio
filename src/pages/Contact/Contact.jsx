import React from 'react';
import { Cubes, Navbar, HomeButton } from '../../components'

import './Contact.scss';

const Contact = () => {
  return (
    <>
      <div className="nav-container">
        <Navbar />
        <HomeButton />
      </div>
      <div className="cubes-container">
        <Cubes />
      </div>
    </>
  )
}

export default Contact