import React, { Suspense } from 'react';
import { Canvas } from '@react-three/fiber'
import { Mirror, Plane, Navbar, HomeButton, PortfolioWeel } from '../../components'
import { Preload } from '@react-three/drei'

import './Portfolio.scss';

const Portfolio = () => {

  return (

    <>
      <Navbar />
      <HomeButton />
      <div className="canvas-container">
        <Canvas shadows camera={{ position: [1.5, -1.6, 4], fov: 110 }}>
          <Suspense fallback={null}>
            <Preload all />
            <ambientLight intensity={0.5} />
            {/* <pointLight intensity={0.5} position={[2, -2, 3]} /> */}
            <directionalLight
              castShadow
              intensity={0.2}
              position={[0, 6, 2]}
              shadow-mapSize-width={1024}
              shadow-mapSize-height={1024}
            />
            <PortfolioWeel />
            <Mirror position={[0, 0, -7]} rotation={[0, -1, 0]} args={[100, 100]} />
            <Plane position={[-20, 0, 15]} rotation={[0, 1, 0]} args={[100, 200]} color={"#bccbde"} />
            <Plane position={[0, -3, 0]} rotation={[(Math.PI / -2), 0, 0]} args={[100, 100]} color={"lightblue"} />
          </Suspense>
        </Canvas>
      </div>
    </>
  )
}

export default Portfolio