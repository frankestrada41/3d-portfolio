import React, { useState } from 'react';
import { Canvas, useLoader } from '@react-three/fiber'
// import Girl from '../../components/Girl/Girl'
import { OrbitControls } from '@react-three/drei'
import Planet1 from '../../components/Planet1/Planet1'
import { Navbar, HomeButton, Sphere } from '../../components'
import { Suspense } from 'react';
import { TextureLoader } from 'three'


import rock from '../../assets/textures/rock.png'
import rocknormalmap from '../../assets/textures/rock-normalmap.png'

import './About.scss'

const About = () => {
  const [planet2, planet2Normalmap] = useLoader(TextureLoader, [rock, rocknormalmap])
  const [detailS, setDetailS] = useState(false);
  const [detailW, setDetailW] = useState(false);

  const handelDetail = () => {
    setDetailS(!detailS);
  }
  const handelDetailW = () => {
    setDetailW(!detailW);
  }

  return (
    <>
      <div className="home-container">

        <Navbar />

        <div className="portfolio-container">
          {!detailW && <div onClick={handelDetail} className={detailS ? "portfolio-container__studies-container-detail" : "portfolio-container__studies-container "}>
            <div className="portfolio-container__titles">Studies</div>
            <div>
              <p>·10/05/2021 - 01/08/2021:</p>
              <p>Full Stack Development Bootcamp – Upgrade Hub</p>
            </div>
            <div>
              <p>·20/09/2016 - 20/07/2018: </p>
              <p>Producción de audio (ingeniería de sonido) - SAE Institute Barcelona</p>
            </div>
            <div>
              <p>·20/09/2014 - 23/06/2016:</p>
              <p>Técnico de sonido - CEF Mallorca (Palma de Mallorca)</p>
            </div>
          </div>}
          {!detailS && <div onClick={handelDetailW} className={detailW ? 'portfolio-container__work-container-detail' : "portfolio-container__work-container"}>
            <div className="portfolio-container__titles">Work Experience</div>
            <div>
              <p>·2019 - 2021:</p>
              <p>Técnico de sonido y multimedia (SDI Media, Barcelona)</p>
            </div>
            <div>
              <p>·2018 - 2019: </p>
              <p>Aprendiz de electricista (Barcelona)</p>
            </div>
            <div>
              <p>·2016 - 2018:</p>
              <p>Montaje de equipo técnico (Gallowglass, Barcelona)</p>
            </div>
            <div>
              <p>·2015 - 2016:</p>
              <p>Técnico de sonido TV (Prácticas) (Canal 4, Mallorca)</p>
            </div>
          </div>}
        </div>
        <div className="back-container">
          <HomeButton />
        </div>
      </div>
      <div className="model-container">
        <Canvas camera={{ position: [0, 0, 8] }}>
          <Suspense fallback={null}>
            <pointLight color="#f6f3ea" position={[17, 1, 12]} intensity={0.7} />
            <pointLight color="#db76db" position={[-2, -1, -6]} intensity={0.5} />
            <Planet1 />
            <Sphere position={[0, 1, -8]} args={[0.4, 32, 32]} map={planet2} normalMap={planet2Normalmap} />
          </Suspense>
          <OrbitControls />
        </Canvas>
      </div>
    </>
  )
}

export default About;