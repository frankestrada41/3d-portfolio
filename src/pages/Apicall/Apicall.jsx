import React from 'react';
import { Navbar, HomeButton, Apicanvas } from '../../components'

import './Apicall.scss'
const Apicall = () => {

  return (
    <>
      <Navbar />
      <HomeButton />
      <Apicanvas />
    </>
  );
};

export default Apicall;